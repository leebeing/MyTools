﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTools
{
    class MyTool
    {
        public static string ConvertStringToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                try
                {
                    int tmp = c;
                    hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
                catch (System.Exception ex)
                {
                    hex = ex.ToString();
                }
                
            }
            return hex;
        }

        public static string ConvertHexToString(string HexValue)
        {
            string StrValue = "";
            while (HexValue.Length > 0)
            {
                try
                {
                    StrValue += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
                    HexValue = HexValue.Substring(2, HexValue.Length - 2);
                }
                catch (System.Exception ex)
                {
                    StrValue = ex.ToString();
                    break;
                }

            }
            return StrValue;
        }
    }
}
