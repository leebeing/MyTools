﻿namespace MyTools
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MyToolTab = new System.Windows.Forms.TabControl();
            this.ASCII_HEX = new System.Windows.Forms.TabPage();
            this.button_Clear = new System.Windows.Forms.Button();
            this.button_HexToAscii = new System.Windows.Forms.Button();
            this.button_AsciiToHex = new System.Windows.Forms.Button();
            this.textBox_Out = new System.Windows.Forms.TextBox();
            this.textBox_Input = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.MyToolTab.SuspendLayout();
            this.ASCII_HEX.SuspendLayout();
            this.SuspendLayout();
            // 
            // MyToolTab
            // 
            this.MyToolTab.Controls.Add(this.ASCII_HEX);
            this.MyToolTab.Controls.Add(this.tabPage2);
            this.MyToolTab.Location = new System.Drawing.Point(0, 0);
            this.MyToolTab.Name = "MyToolTab";
            this.MyToolTab.SelectedIndex = 0;
            this.MyToolTab.Size = new System.Drawing.Size(558, 287);
            this.MyToolTab.TabIndex = 0;
            this.MyToolTab.Selected += new System.Windows.Forms.TabControlEventHandler(this.MyToolTab_Selected);
            // 
            // ASCII_HEX
            // 
            this.ASCII_HEX.Controls.Add(this.button_Clear);
            this.ASCII_HEX.Controls.Add(this.button_HexToAscii);
            this.ASCII_HEX.Controls.Add(this.button_AsciiToHex);
            this.ASCII_HEX.Controls.Add(this.textBox_Out);
            this.ASCII_HEX.Controls.Add(this.textBox_Input);
            this.ASCII_HEX.Location = new System.Drawing.Point(4, 21);
            this.ASCII_HEX.Name = "ASCII_HEX";
            this.ASCII_HEX.Padding = new System.Windows.Forms.Padding(3);
            this.ASCII_HEX.Size = new System.Drawing.Size(550, 262);
            this.ASCII_HEX.TabIndex = 0;
            this.ASCII_HEX.Text = "ASCII HEX";
            this.ASCII_HEX.UseVisualStyleBackColor = true;
            this.ASCII_HEX.Validated += new System.EventHandler(this.ASCII_HEX_Validated);
            // 
            // button_Clear
            // 
            this.button_Clear.Location = new System.Drawing.Point(113, 108);
            this.button_Clear.Name = "button_Clear";
            this.button_Clear.Size = new System.Drawing.Size(75, 23);
            this.button_Clear.TabIndex = 4;
            this.button_Clear.Text = "清空";
            this.button_Clear.UseVisualStyleBackColor = true;
            this.button_Clear.Click += new System.EventHandler(this.button_Clear_Click);
            // 
            // button_HexToAscii
            // 
            this.button_HexToAscii.Location = new System.Drawing.Point(329, 108);
            this.button_HexToAscii.Name = "button_HexToAscii";
            this.button_HexToAscii.Size = new System.Drawing.Size(75, 23);
            this.button_HexToAscii.TabIndex = 3;
            this.button_HexToAscii.Text = "Hex转ASCII";
            this.button_HexToAscii.UseVisualStyleBackColor = true;
            this.button_HexToAscii.Click += new System.EventHandler(this.button_HexToAscii_Click);
            // 
            // button_AsciiToHex
            // 
            this.button_AsciiToHex.Location = new System.Drawing.Point(206, 108);
            this.button_AsciiToHex.Name = "button_AsciiToHex";
            this.button_AsciiToHex.Size = new System.Drawing.Size(75, 23);
            this.button_AsciiToHex.TabIndex = 2;
            this.button_AsciiToHex.Text = "ASCII转Hex";
            this.button_AsciiToHex.UseVisualStyleBackColor = true;
            this.button_AsciiToHex.Click += new System.EventHandler(this.button_AsciiToHex_Click);
            // 
            // textBox_Out
            // 
            this.textBox_Out.Location = new System.Drawing.Point(0, 137);
            this.textBox_Out.Multiline = true;
            this.textBox_Out.Name = "textBox_Out";
            this.textBox_Out.ReadOnly = true;
            this.textBox_Out.Size = new System.Drawing.Size(550, 122);
            this.textBox_Out.TabIndex = 1;
            // 
            // textBox_Input
            // 
            this.textBox_Input.Location = new System.Drawing.Point(3, 6);
            this.textBox_Input.Multiline = true;
            this.textBox_Input.Name = "textBox_Input";
            this.textBox_Input.Size = new System.Drawing.Size(547, 94);
            this.textBox_Input.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(550, 262);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 285);
            this.Controls.Add(this.MyToolTab);
            this.Name = "Form1";
            this.Text = "我的工具 - LeeBeing";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.MyToolTab.ResumeLayout(false);
            this.ASCII_HEX.ResumeLayout(false);
            this.ASCII_HEX.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MyToolTab;
        private System.Windows.Forms.TabPage ASCII_HEX;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button_HexToAscii;
        private System.Windows.Forms.Button button_AsciiToHex;
        private System.Windows.Forms.TextBox textBox_Out;
        private System.Windows.Forms.TextBox textBox_Input;
        private System.Windows.Forms.Button button_Clear;
    }
}

