﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyTools
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_AsciiToHex_Click(object sender, EventArgs e)
        {
            if (textBox_Input.TextLength>0)
            {
                textBox_Out.Text = MyTool.ConvertStringToHex(textBox_Input.Text);
            }
            else
            {
                MessageBox.Show("输入待转换的字串");
            }
        }

        private void button_HexToAscii_Click(object sender, EventArgs e)
        {
            if (textBox_Input.TextLength>0)
            {
                textBox_Out.Text = MyTool.ConvertHexToString(textBox_Input.Text);
            }
            else
            {
                MessageBox.Show("输入待转换的字串");
            }
        }

        private void button_Clear_Click(object sender, EventArgs e)
        {
            textBox_Input.Text = string.Empty;
            textBox_Out.Text = string.Empty;
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            textBox_Input.Focus();
        }

        private void MyToolTab_Selected(object sender, TabControlEventArgs e)
        {
        }

        private void ASCII_HEX_Validated(object sender, EventArgs e)
        {
            textBox_Input.Focus();
        }
    }
}
